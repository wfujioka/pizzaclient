package com.ganchallenge.pizzaclient.controller;

import java.util.Set;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ganchallenge.pizzaclient.repository.OrdersRepository;

@RequestMapping("/orders")
@RestController
public class OrderController {

  private OrdersRepository ordersRepo;

  public OrderController(OrdersRepository ordersRepo) {
    this.ordersRepo = ordersRepo;
  }

  @PostMapping("/completed/{messageId}")
  public String receiveCompletedOrder(@PathVariable String messageId) {
    ordersRepo.addToCompletedSet(messageId);
    return "{status: RECEIVED}";
  }

  @GetMapping("/completed/view")
  public Set<String> viewCompletedOrders() {
    return ordersRepo.getOrderCompletedSet();
  }
}
