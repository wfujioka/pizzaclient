package com.ganchallenge.pizzaclient.dto;

public enum EscapeOptions {
  EXIT, REDO;
}
