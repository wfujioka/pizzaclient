package com.ganchallenge.pizzaclient.dto;

import java.util.LinkedList;
import java.util.Queue;
import org.springframework.stereotype.Component;

@Component
public class OrderRequest {
  private Queue<Order> orderQueue;

  public OrderRequest() {
    orderQueue = new LinkedList<>();
  }

  public Queue<Order> getOrderQueue() {
    return orderQueue;
  }

  public void setOrderQueue(Queue<Order> orderQueue) {
    this.orderQueue = orderQueue;
  }
}
