package com.ganchallenge.pizzaclient.services;

import java.text.MessageFormat;
import java.time.Duration;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import com.ganchallenge.pizzaclient.dto.Order;
import com.ganchallenge.pizzaclient.dto.OrderResponse;

@Component
public class PizzaServerClient {

  private static final Logger log = LoggerFactory.getLogger(PizzaServerClient.class);

  private RestTemplate restTemplate;

  @Value("${pizza-client.pizza-server-url}")
  private String pizzaServerUrl;

  public PizzaServerClient(RestTemplateBuilder restTemplateBuilder) {
    this.restTemplate = restTemplateBuilder.setConnectTimeout(Duration.ofHours(1)).setReadTimeout(Duration.ofHours(1)).build();
  }

  @SuppressWarnings("unchecked")
  public Map<String, Integer> getAvailableIngredients() {
    String url = MessageFormat.format("{0}/ingredients/available", pizzaServerUrl);
    return restTemplate.getForObject(url, Map.class);
  }

  @SuppressWarnings("unchecked")
  public boolean validateIngredients(Map<String, Integer> ingredientsMap) {
    String url = MessageFormat.format("{0}/ingredients/verify", pizzaServerUrl);
    Map<String, Boolean> response = restTemplate.postForObject(url, ingredientsMap, Map.class);
    return response.get("isValid");
  }

  public OrderResponse sendOrder(Order order) {
    String url = MessageFormat.format("{0}/orders/submit", pizzaServerUrl);
    return restTemplate.postForObject(url, order, OrderResponse.class);
  }

  @SuppressWarnings("unchecked")
  public boolean isInQueueLongPolling(String msgId) {
    String url = MessageFormat.format("{0}/orders/long-poll-until-not-in-queue/{1}", pizzaServerUrl, msgId);
    Map<String, String> response = restTemplate.getForObject(url, Map.class);
    if (response != null && "IN_QUEUE".equals(response.get("status"))) {
      return true;
    }
    return false;
  }

}
