package com.ganchallenge.pizzaclient.services;

import java.net.InetAddress;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.ganchallenge.pizzaclient.repository.OrdersRepository;

@Service
public class OrdersService {

  private OrdersRepository ordersRepo;
  private String orderCompletedUrl;

  @Value("${server.port}")
  private String serverPort;
  @Value("${server.servlet.context-path}")
  private String contextPath;


  public OrdersService(OrdersRepository ordersRepo) {
    this.ordersRepo = ordersRepo;
  }

  @PostConstruct
  public void init() {
    generateOrderCompletedUrl();
  }

  public void addToCompletedSet(String msgId) {
    ordersRepo.addToCompletedSet(msgId);
  }

  public boolean hasOrderBeenCompleted(String msgId) {
    return ordersRepo.removeFromCompletedSet(msgId);
  }

  public Set<String> getOrdersCompletedSet() {
    return ordersRepo.getOrderCompletedSet();
  }

  public String getOrderCompletedUrl() {
    return orderCompletedUrl;
  }

  private void generateOrderCompletedUrl() {
    String hostName = InetAddress.getLoopbackAddress().getHostName();
    String baseUri = "http://" + hostName + ":" + serverPort + contextPath;
    orderCompletedUrl = baseUri + "/orders/completed/";
  }

}
