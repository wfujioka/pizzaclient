package com.ganchallenge.pizzaclient;

import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.stream.IntStream;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import com.ganchallenge.pizzaclient.dto.EscapeOptions;
import com.ganchallenge.pizzaclient.dto.Order;
import com.ganchallenge.pizzaclient.dto.OrderRequest;
import com.ganchallenge.pizzaclient.dto.OrderResponse;
import com.ganchallenge.pizzaclient.services.OrdersService;
import com.ganchallenge.pizzaclient.services.PizzaServerClient;

@Component
public class ClientTerminal implements CommandLineRunner {

  private OrderRequest orderRequest;
  private ApplicationContext appContext;
  private EscapeOptions escapeOpt;
  private PizzaServerClient pizzaServerClient;
  private OrdersService ordersService;

  public ClientTerminal(OrderRequest orderRequest, ApplicationContext appContext, PizzaServerClient pizzaServerClient, OrdersService ordersService) {
    this.orderRequest = orderRequest;
    this.appContext = appContext;
    this.pizzaServerClient = pizzaServerClient;
    this.ordersService = ordersService;
  }

  @Override
  public void run(String... args) throws Exception {

    System.out.println("=====================================");
    System.out.println("To shutdown application, type 'exit' at anytime");
    System.out.println("To start over, type 'redo' at anytime");
    System.out.println("=====================================");

    try (Scanner scanner = new Scanner(System.in)) {
      do {
        escapeEscapeOptions();
        System.out.println("Greetings!");

        requestPizzaCount(scanner);
        if (isResetOrderOrShutdown()) {
          continue;
        }

        makeOrderRequest(scanner);
        if (isResetOrderOrShutdown()) {
          continue;
        }

      } while (escapeOpt != EscapeOptions.EXIT);
    }
  }

  public void makeOrderRequest(Scanner scanner) {
    System.out.println("=====================================");
    System.out.println("Choose your toppings for your pizza(s)");
    System.out.println("Please add ingredients with quantity, separated by commas");
    System.out.println("i.e. mushrooms 2, cheese 1, tomato 1, etc\n");

    Queue<Order> orderQueue = orderRequest.getOrderQueue();
    for (int i = 1; orderQueue.size() > 0; i++) {

      boolean isIndividualOrderComplete = false;
      System.out.println("Available toppings at this time:");
      displayAvailableToppings();

      Order order = orderQueue.remove();
      do {
        System.out.println("\nPlease enter your toppings for pizza " + i);
        System.out.println("=====================================");
        if (scanner.hasNext()) {
          String output = scanner.nextLine();
          if (!StringUtils.hasText(output)) {
            continue;
          }

          if (isShutDownRequested(output)) {
            escapeOpt = EscapeOptions.EXIT;
            return;
          }

          if (isAbortOrderRequested(output)) {
            escapeOpt = EscapeOptions.REDO;
            return;
          }

          Map<String, Integer> ingredients = generateIngredientMap(output);
          if (!validateToppingOrder(ingredients)) {
            System.out.println("Invalid topping request, Please try again...");
            continue;
          }
          fillOrder(order, ingredients);
          OrderResponse orderResponse = pizzaServerClient.sendOrder(order);
          if (orderResponse == null || !StringUtils.hasText(orderResponse.getMessageId())) {
            System.out.println("Unable to process your order.  Please try again...");
          }
          System.out.println("Order was successfully submitted!");
          System.out.println("Waiting til order gets to oven...");
          boolean isInQueue = true;
          do {
            isInQueue = pizzaServerClient.isInQueueLongPolling(orderResponse.getMessageId());
          } while (isInQueue);

          System.out.println("Order is now baking in the oven...");
          boolean isCompleted = false;
          int bakeTime = orderResponse.getBakeTime();
          System.out.print("Countdown: ");
          for (int j = 0; !isCompleted; j++) {

            if (j == 0) {
              System.out.print(bakeTime);
            } else {
              System.out.print(", " + (bakeTime - j));
            }
            try {
              Thread.sleep(1000);
            } catch (InterruptedException e) {
              // ignore
            }
            isCompleted = ordersService.hasOrderBeenCompleted(orderResponse.getMessageId());
          }
          System.out.println("\nPizza " + i + " order had finished baking and is ready to be picked up.");
          System.out.println("\n=====================================");
          isIndividualOrderComplete = true;
        }
      } while (!isIndividualOrderComplete);
    }

  }

  private boolean validateToppingOrder(Map<String, Integer> ingredients) {
    if (ingredients == null) {
      return false;
    }
    return pizzaServerClient.validateIngredients(ingredients);
  }

  private Map<String, Integer> generateIngredientMap(String orderOutput) {
    Map<String, Integer> ingredientsMap = new HashMap<>();
    String[] ingredientsArr = orderOutput.split(",");
    for (String ingredients : ingredientsArr) {
      String[] ingredientAndQuantity = ingredients.trim().split(" ");
      if (ingredientAndQuantity.length != 2) {
        return null;
      }
      String ingredient = ingredientAndQuantity[0];
      Integer quantity = -1;
      try {
        quantity = Integer.parseInt(ingredientAndQuantity[1]);
      } catch (NumberFormatException ne) {
        return null;
      }
      ingredientsMap.put(ingredient, quantity);
    }
    return ingredientsMap;
  }

  private Order fillOrder(Order order, Map<String, Integer> ingredients) {
    order.setUrl(ordersService.getOrderCompletedUrl());
    order.setIngredients(ingredients);
    return order;
  }

  private void displayAvailableToppings() {
    pizzaServerClient.getAvailableIngredients().entrySet().forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
  }

  public void requestPizzaCount(Scanner scanner) {
    boolean isCompleted = false;

    do {
      System.out.println("=====================================");
      System.out.println("How many pizza's would you like to order?");
      System.out.println("=====================================");
      if (scanner.hasNext()) {
        String output = scanner.next();
        if (isShutDownRequested(output)) {
          escapeOpt = EscapeOptions.EXIT;
          return;
        }

        if (isAbortOrderRequested(output)) {
          escapeOpt = EscapeOptions.REDO;
          return;
        }

        Integer pizzaCount = getNumericValue(output);
        if (pizzaCount != null) {
          if (pizzaCount < 1) {
            System.out.println("Your must order must consist of 1 or more pizzas.");
          } else {
            IntStream.range(0, pizzaCount).forEach(i -> addEmptyPizzaOrdersToRequest());
            isCompleted = true;
          }
        } else {
          System.out.println("Sorry, I did not understand that");
        }
      }
    } while (!isCompleted);

  }

  public void addEmptyPizzaOrdersToRequest() {
    Queue<Order> pizzaQueue = orderRequest.getOrderQueue();
    pizzaQueue.add(appContext.getBean(Order.class));
  }

  private Integer getNumericValue(String str) {
    if (!StringUtils.hasText(str)) {
      return null;
    }

    try {
      return Integer.parseInt(str);
    } catch (NumberFormatException e) {
      // do nothing
    }
    return null;
  }

  private void escapeEscapeOptions() {
    escapeOpt = null;
  }

  private boolean isShutDownRequested(String consoleOutput) {
    return "exit".equalsIgnoreCase(consoleOutput);
  }

  private boolean isAbortOrderRequested(String consoleOutput) {
    return "redo".equalsIgnoreCase(consoleOutput);
  }

  private boolean isResetOrderOrShutdown() {
    return escapeOpt == EscapeOptions.EXIT || escapeOpt == EscapeOptions.REDO;
  }
}
