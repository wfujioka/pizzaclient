package com.ganchallenge.pizzaclient.repository;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import org.springframework.stereotype.Repository;

@Repository
public class OrdersRepository {

  private Set<String> orderCompletedSet;

  public OrdersRepository() {
    orderCompletedSet = Collections.synchronizedSet(new HashSet<>());
  }

  public void addToCompletedSet(String msgId) {
    orderCompletedSet.add(msgId);
  }

  public boolean removeFromCompletedSet(String msgId) {
    return orderCompletedSet.remove(msgId);
  }

  public Set<String> getOrderCompletedSet() {
    return orderCompletedSet;
  }

}
